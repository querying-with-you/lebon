# Nobel Laureates Search Engine

Final project of semantic web course.

## Querying With You

- Gagah Pangeran Rosfatiputra - 170639566
- Muhamad Abdurahman - 1706040095
- Muhamad Achir Suci Ramadhan - 1706979354

## Live Demo

[https://lebon.netlify.com](https://lebon.netlify.com)

## Triple Store & Sparql Server

[https://fuseki.gagahpangeran.com](https://fuseki.gagahpangeran.com)
